package services;

import dto.TestUser;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class UserService extends APIService{

    public static void createUser(TestUser user) {
        setup()
                .body(user)
                .when()
                .post("user")
                .then()
                .assertThat()
                .statusCode(200);
    }
    public static TestUser getUserByName(String userName) {
        return setup()
                .when()
                .get("user/".concat(userName))
                .then()
                .assertThat()
                .statusCode(200)
                .extract().as(TestUser.class);

    }



}
