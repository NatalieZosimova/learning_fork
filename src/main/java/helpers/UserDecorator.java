package helpers;

import com.github.javafaker.Faker;
import java.util.Locale;
import dto.TestUser;


public class UserDecorator {
    public static Faker faker = new Faker(new Locale("en-US"));

    public TestUser fillData(TestUser testUser) {
        if (isNoValue(testUser.getId())) testUser.setId(faker.number().numberBetween(0, 2147483646));
        if (isNoValue(testUser.getUsername())) testUser.setUsername(faker.name().username());
        if (isNoValue(testUser.getFirstName())) testUser.setFirstName(faker.name().firstName());
        if (isNoValue(testUser.getLastName())) testUser.setLastName(faker.name().lastName());
        if (isNoValue(testUser.getEmail())) testUser.setEmail(faker.internet().emailAddress());
        if (isNoValue(testUser.getPassword())) testUser.setPassword(faker.internet().password());
        if (isNoValue(testUser.getAddress())) testUser.setAddress(faker.address().streetAddress());
        if (isNoValue(testUser.getCity())) testUser.setCity(faker.address().city());
        if (isNoValue(testUser.getState())) testUser.setState(faker.address().state());
        if (isNoValue(testUser.getPostcode()))  testUser.setPostcode(faker.number().digits(5));
        if (isNoValue(testUser.getPhone())) testUser.setPhone(faker.phoneNumber().cellPhone());
        if (isNoValue(testUser.getAddressAlias())) testUser.setAddressAlias(faker.funnyName().name());
        if (isNoValue(testUser.getUserStatus())) testUser.setUserStatus(faker.number().randomDigit());

        return testUser;
    }

    private Boolean isNoValue(Object param) {
        return (param == null) || (String.valueOf(param).isEmpty())||(String.valueOf(param).isBlank());
    }

}
