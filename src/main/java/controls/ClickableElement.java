package controls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ClickableElement extends BaseElement implements IClickable {
    public ClickableElement(WebDriver driver, By locator) {
        super(driver, locator);
    }

    @Override
    public void click() {
        getElement().click();
    }
}
