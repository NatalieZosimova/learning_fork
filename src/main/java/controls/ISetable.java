package controls;

public interface ISetable {
    void setText(String text);
}
