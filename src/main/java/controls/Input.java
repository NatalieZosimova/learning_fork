package controls;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Input extends BaseElement implements IClickable, ISetable {

    public Input(WebDriver driver, By locator) {
        super(driver, locator);
    }

    @Override
    public void click() {
        getElement().click();
    }

    @Override
    public void setText(String text) {
        getElement().sendKeys(text);
    }
}
