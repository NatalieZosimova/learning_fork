package pages;

import controls.*;
import dto.TestUser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class LoginPage extends BasePage {

    private Input emailForCreate;
    private ClickableElement startCreationBtn;
    private ClickableElement submitNewAccBtn;

    private ClickableElement genderRBMale;
    private Input customerFirstnameField;
    private Input customerLastnameField;
    private Input passwordField;
    private Input addressField;
    private Input cityField;
    private DropDown stateField;
    private Input postcodeField;
    private Input mobilePhoneField;
    private Input aliasField;

    public LoginPage(WebDriver driver) {
        super(driver);
        emailForCreate = new Input(getDriver(), By.id("email_create"));
        startCreationBtn = new ClickableElement(getDriver(), By.id("SubmitCreate"));
        submitNewAccBtn = new ClickableElement(getDriver(), By.id("submitAccount"));
        genderRBMale = new ClickableElement(getDriver(), By.id("id_gender1"));
        customerFirstnameField = new Input(getDriver(), By.id("customer_firstname"));
        customerLastnameField = new Input(getDriver(), By.id("customer_lastname"));
        passwordField = new Input(getDriver(), By.id("passwd"));
        addressField = new Input(getDriver(), By.id("address1"));
        cityField = new Input(getDriver(), By.id("city"));
        stateField = new DropDown(getDriver(), By.id("id_state"));
        postcodeField = new Input(getDriver(), By.id("postcode"));
        mobilePhoneField = new Input(getDriver(), By.id("phone_mobile"));
        aliasField = new Input(getDriver(), By.id("alias"));
    }

    public LoginPage inputEmailToCreateAccount(String email) {
        emailForCreate.setText(email);
        startCreationBtn.click();
        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.id("account-creation_form")));
        return this;
    }

    public WebElement getWelcomeLabel() {
        return new ReadableElement(getDriver(), By.xpath("//p[@class=\"info-account\"]"))
                .getElement();
    }

    public LoginPage fillRegisterForm(TestUser user) {
        genderRBMale.click();
        customerFirstnameField.setText(user.getFirstName());
        customerLastnameField.setText(user.getLastName());
        passwordField.setText(user.getPassword());
        addressField.setText(user.getAddress());
        cityField.setText(user.getCity());
        stateField.setText(user.getState());
        postcodeField.setText(user.getPostcode());
        mobilePhoneField.setText(user.getPhone());
        aliasField.setText(user.getAddressAlias());

        submitNewAccBtn.click();

        getWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[@class=\"info-account\"]")));
        // TODO create AccPage
        return this;

    }


}
